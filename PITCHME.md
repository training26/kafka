# Kafka

Kafka in Pricefx

#HSLIDE

## Kafka Design

- Kafka Design
- Confluent Cloud
- Implementation in Platform Manager
- Implementation in Integration Manager

#HSLIDE

## Kafka Design

- Topic as distrubuted log (Unordered)
![Topic anatomy](https://kafka.apache.org/23/images/log_anatomy.png "Topic anatomy")
- Partitions as distribution unit
    - Ordered, Key

#HSLIDE

## Kafka Design
- Consumer Group
![Consumer Group](https://kafka.apache.org/23/images/consumer-groups.png "Consumer Group")
- Queue

#HSLIDE

## Kafka Design - Offset
![offset](https://kafka.apache.org/23/images/log_consumer.png "offset")
- May or may not be stored
- We store offset in separate Kafka topic

#HSLIDE

## Kafka Design - Pros and Cons
### Pros
- Almost linar scalability

### Cons
- Partition management - ordering
- Ops - Zookeeper
- Offset management
- Consumer Groups management

#HSLIDE

## Kafka Design - Common Use Cases
- Messaging
- Stream Processing
- Web app activity tracking
- Event Sourcing
- Commit log

#HSLIDE

## Confluent Cloud

- Apache Kafka as a service
    - Autoscaling
    - Pay as you go (tranfered and stored data)
- Environment
    - prod, qa 
- Cluster
    - PlatformCommunication 
- Topic
    - integration-manager-events (6 partitions)
    - retention - time & size

#HSLIDE

## Confluent Cloud Web Management

- Less powerfull that CLI tool
    - Except Billing and Payments
- DEMO https://confluent.cloud/

#HSLIDE

## Confluent Cloud CLI Management

- CLI tool for managing Confluent Cloud
- ACLs
- Gives you greater power that WEB managent, but with a great power...

- DEMO

#HSLIDE

## Implementation Pricefx

- Event driven style
    - IM_STARTUP, IM_SHUTDOWN, FILE_PROCESSED

- Asynchronous messaging

#HSLIDE

## Implementation in Integration Manager

[KafkaEventPublisher](https://bitbucket.org/pricefx/pricefx-integration-commons/src/develop/integration-platform/integration-autoconfigurator/src/main/java/net/pricefx/integration/autoconfigure/eventdriven/KafkaEventPublisher.java)

#HSLIDE

## Implementation in Platform Manager

- Spring Cloud Stream consumer
    - Unmarshalls event from JSON
    - Publish event via Spring ApplicationEventPublisher
    - Any Spring component in PM can subscribe

[MessagingService](https://bitbucket.org/pricefx/pricefx-integration-manager-ui/src/develop/integration-config-server/src/main/java/net/pricefx/integration/config/server/service/MessagingService.java)


